# A '0' is prepended to this filename so that it executes first in the initializers, allowing us to use
# the configuration loaded here in the other initializers.

APP_CONFIG = YAML.load_file("#{Rails.root}/config/app_config.yml").deep_symbolize_keys