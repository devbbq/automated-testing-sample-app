class CreateOutOfOffices < ActiveRecord::Migration
  def change
    create_table :out_of_offices do |t|
      t.date :date
      t.string :description
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
