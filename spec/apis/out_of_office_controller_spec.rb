require 'rails_helper'
require 'airborne'

RSpec.describe OutOfOfficesController, type: :controller do

  describe 'API authentication' do

    it 'should respond with unauthorized when user api token is absent' do
      get :test
      expect_status 401
    end

    it 'should respond with unauthorized when user api token is incorrect' do
      # no users present
      get :test, { api_token: '12345' }
      expect_status 401

      # user present
      create(:user)
      get :test, { api_token: '12345' }
      expect_status 401
    end

    it 'should respond with 200 when the request includes a valid user api token' do
      get :test, { api_token: create(:user).api_token }
      expect_status 200
    end

  end

  describe 'creating records' do

    it 'should create a record for a user' do
      user = create(:user)

      post :create, { api_token: user.api_token, date_string: 'tomorrow' }
      expect_status 200

      expect(OutOfOffice.count).to eq(1)

      oo = OutOfOffice.first

      expect(oo.user_id).to eq(user.id)
      expect(oo.date).to eq(Date.tomorrow)
      expect(oo.reason).to eql('wfh')
    end

  end

end

