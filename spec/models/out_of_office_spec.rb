# == Schema Information
#
# Table name: out_of_offices
#
#  id          :integer          not null, primary key
#  date        :date
#  description :string
#  user_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_out_of_offices_on_user_id  (user_id)
#

require 'rails_helper'

RSpec.describe OutOfOffice, type: :model do

  describe 'validation' do

    it 'should have an associated user' do
      expect(create(:out_of_office).user.id).to eq(User.first.id)
      expect(build(:out_of_office, user_id: nil)).not_to be_valid
    end

    it 'should have a date' do
      expect(build(:out_of_office, date: nil)).not_to be_valid
    end

    it 'should not have a date in the past' do
      expect(build(:out_of_office, date: Date.yesterday)).not_to be_valid
      expect(create(:out_of_office, date: Date.tomorrow)).to be_valid
    end

    it 'should have a description if on the work from office date' do
      date = Date.parse(APP_CONFIG[:work_from_office_day])
      delta = date > Date.today ? 0 : 7
      next_monday = date + delta

      expect(build(:out_of_office, date: next_monday)).not_to be_valid
      expect(create(:out_of_office, date: next_monday, description: "Working at client's Toronto office")).to be_valid
    end

  end

  describe 'Out of office reports' do

    it 'should report working from home unless there is a description present' do
      expect(create(:out_of_office).reason).to eq('wfh')
      expect(create(:out_of_office, description: 'Vacation').reason).to eq('Vacation')
    end

    it 'should list who is out of the office with a reason' do

      user_1 = create(:user)
      user_2 = create(:user)
      user_3 = create(:user)

      create(:out_of_office, user_id: user_1.id)
      create(:out_of_office, user_id: user_2.id)
      create(:out_of_office, user_id: user_3.id, date: Date.tomorrow, description: 'Vacation')

      expect(User.count).to eq(3)

      oo_today = OutOfOffice.who(Date.today)
      expect(oo_today.count).to eq(2)
      expect(oo_today[user_1.name]).to eq('wfh')
      expect(oo_today[user_2.name]).to eq('wfh')

      oo_tomorrow = OutOfOffice.who(Date.tomorrow)
      expect(oo_tomorrow.count).to eq(1)
      expect(oo_tomorrow[user_3.name]).to eq('Vacation')
    end

  end

end
