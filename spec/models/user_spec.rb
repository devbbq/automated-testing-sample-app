# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  name       :string
#  api_token  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe User, type: :model do

  describe 'validation' do

    it 'must have a name' do
      expect(build(:user, name: nil)).not_to be_valid
    end

    it 'must have a unique name' do
      expect(create(:user)).to be_valid
      name = User.first.name
      expect(build(:user, name: name)).not_to be_valid
    end

  end

end
