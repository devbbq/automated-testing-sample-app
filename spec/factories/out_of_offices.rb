# == Schema Information
#
# Table name: out_of_offices
#
#  id          :integer          not null, primary key
#  date        :date
#  description :string
#  user_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_out_of_offices_on_user_id  (user_id)
#

FactoryGirl.define do
  factory :out_of_office do
    user
    date { Date.today }
    description nil
  end

end
