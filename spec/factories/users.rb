# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  name       :string
#  api_token  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :user do
    name { Faker::Name.first_name }
    api_token { SecureRandom.uuid }
  end

end
