Automated Testing - Sample Application
=============================================

### System dependencies
- RVM or rbenv

### Commands
- Run all examples: `rspec`
- Start Guard: `bundle exec guard`

### Links
- [RSpec documentation](http://rspec.info/documentation/)
- [RSpec expectations](https://github.com/rspec/rspec-expectations)
- [FactoryGirl (getting started)](https://github.com/thoughtbot/factory_girl/blob/master/GETTING_STARTED.md)
- [DatabaseCleaner](https://github.com/DatabaseCleaner/database_cleaner)
- [guard-rspec](https://github.com/guard/guard-rspec)
- [airborne](https://github.com/brooklynDev/airborne)

### Other useful testing gems
- [email-spec](https://github.com/bmabey/email-spec)
- [faker](https://github.com/stympy/faker)
- [vcr](https://github.com/vcr/vcr)
- [timecop](https://github.com/travisjeffery/timecop)
- [capybara](https://github.com/jnicklas/capybara)
- [launchy](https://github.com/copiousfreetime/launchy)
