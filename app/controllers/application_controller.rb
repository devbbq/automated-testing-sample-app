class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  before_filter :api_authenticate_user

  private

  def api_authenticate_user
    begin
      raise 'User not authenticated' if User.count.zero?
      @current_user = User.find_by_api_token!(params[:api_token])
    rescue StandardError => e
      user_not_authenticated
    end
  end

  def user_not_authenticated
    render json: {
             data: {}
           }, status: 401
  end

end
