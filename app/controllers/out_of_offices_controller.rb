class OutOfOfficesController < ApplicationController

  def test
    render json: {
             data: {}
           }, status: 200
  end

  def create
    date = Chronic.parse(params[:date_string]).to_date

    out_of_office = @current_user.out_of_offices.new date: date

    if out_of_office.save
      render json: {
               data: {}
             }, status: 200
    end

  end

end
