# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  name       :string
#  api_token  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class User < ActiveRecord::Base

  #------------------------------------------------------------------------------
  # Validations
  validates :name, presence: true, uniqueness: true
  validates :api_token, presence: true, uniqueness: true

  #------------------------------------------------------------------------------
  # Associations
  has_many :out_of_offices

end
