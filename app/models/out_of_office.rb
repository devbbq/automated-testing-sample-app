# == Schema Information
#
# Table name: out_of_offices
#
#  id          :integer          not null, primary key
#  date        :date
#  description :string
#  user_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_out_of_offices_on_user_id  (user_id)
#

class OutOfOffice < ActiveRecord::Base

  #------------------------------------------------------------------------------
  # Validations
  validates :user_id, presence: true
  validates :date, presence: true
  validate :date_cannot_be_in_the_past, :description_required_if_work_from_office_day

  #------------------------------------------------------------------------------
  # Associations
  belongs_to :user

  #------------------------------------------------------------------------------
  # Class methods

  def self.who(date)
    out_of_offices = {}
    OutOfOffice.where(date: date).each{ |oo| out_of_offices.merge!({ oo.user.name => oo.reason }) }
    out_of_offices
  end

  #------------------------------------------------------------------------------
  # Instance methods

  def reason
    description.nil? ? 'wfh' : description
  end

  private

  def date_cannot_be_in_the_past
    if date.present? && date < Date.today
      errors.add(:date, "can't be in the past")
    end
  end

  def description_required_if_work_from_office_day
    if date.present? && date.strftime('%A').eql?(APP_CONFIG[:work_from_office_day]) && description.nil?
      errors.add(:date, "#{APP_CONFIG[:work_from_office_day]} means get your ass to the office")
    end
  end

end
